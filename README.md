Git Ssh Commands for PowerShell
===============================

[Git for Windows][1] comes bundled with a pretty useful compilation of
OpenSSH. Unfortunately, it's stored in the same directory where all their
other tools they warn about in install are also stored (find.exe, sort.exe,
etc). This prevents you from simply adding the directory to your PATH
environment variable without fully understanding the consequences.

While Microsoft has a team working on a [Win32 port of OpenSSH][2] it
unfortunately is not fully ready for production use yet and mainly has
unresolved issues pertaining to the console.

This is a stop-gap measure that allows you to use the bundled SSH with Git in
a manner that feels natural to OpenSSH users, without needing to change
command names or remember different arguments.


Installation
------------
Simply clone this repository in to your modules folder. You can find the
current location of this folder by running `Split-Path $profile` in a
PowerShell prompt.


Uninstallation
--------------
Remove the directory storing this module from your modules folder.


Usage
-----
This module exposes a couple Cmdlets, `Enable-GitSshCommands`,
`Start-GitSshAgent`, and `Stop-GitSshAgent`. Running `Enable-GitSshCommands`
will look in the default location for SSH and create a series of `Invoke-*`
commands that map to their corresponding SSH executable. You can override
the directory by providing a path to the `-Path` option.

The `Start-GitSshAgent` and `Stop-GitSshAgent` are helper commands to
properly process the output provided by `ssh-agent` and update the shell's
environment respectively. This allows other SSH commands to find the agent as
they need.

This module tries to be clean by default, and doesn't create the well-known
aliases for SSH unless asked, which is probably what you want. You can do
this by adding the `-AddAliases` switch when running the command. This will
add aliases for `ssh`, `ssh-keygen`, and more.


Examples
--------

Simply enable the Invoke-* commands.

    PS> Enable-GitSshCommands

Add the aliases when adding the commands.

    PS> Enable-GitSshCommands -AddAliases


Remarks
-------
This module dynamically creates script blocks for the commands and therefore
can't "export" the functions and the aliases. This allows for some more
configuration options but looks a bit hackish (because it is).

License
-------
This project is licensed under the [BSD 3-Clause license](LICENSE).

  [1]: https://git-for-windows.github.io/ "Git for Windows"
  [2]: https://github.com/PowerShell/Win32-OpenSSH/ "Win32 port of OpenSSH"

