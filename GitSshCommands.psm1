$script:ErrorActionPreference = "Stop"

<#
.SYNOPSIS
Adds functions for Git's SSH commands for quick access without polluting the
PATH environment variable.

.DESCRIPTION
This Cmdlet attempts to find all of the known SSH commands included with Git
for Windows and creates functions for them using the Invoke-* verb. An
optional switch can be set to enable the convenience aliases so that "ssh"
and the like can be used without needing to remember the Invoke-* functions.

This allows the SSH commands to be accessed from the prompt without needing
to update and pollute the PATH environment variable, as the same directory
also includes other programs, such as sort.exe and find.exe. While this isn't
detrimental to PowerShell itself, if a child process is invoked that uses
those commands, it could cause a failure.

.PARAMETER Path
Provides the base directory containing the ssh.exe and sibling executables.
Defaults to "C:\Program Files\Git\usr\bin".

.PARAMETER AddAliases
Adds convenience aliases to the global scope. This includes "ssh", "sftp",
and "scp" among others. If not set, only the functions using the Invoke verb
will be available.

.INPUTS
None.

.OUTPUTS
None.

.NOTES
Invoking this function will create functions for the following executables in
the Git directory, if found:

    scp.exe         -> Invoke-Scp
    sftp.exe        -> Invoke-Sftp
    ssh.exe         -> Invoke-Ssh
    ssh-add.exe     -> Invoke-SshAdd
    ssh-agent.exe   -> Invoke-SshAgent
    ssh-keygen.exe  -> Invoke-SshKeygen
    ssh-keyscan.exe -> Invoke-SshKeyscan

If the AddAliases switch is set, convenience aliases will be set to match the
original executable name without the .exe extension. (ex. ssh, scp, ssh-add)
#>
function Enable-GitSshCommands {
    [CmdletBinding(PositionalBinding=$false)]
    param(
        [Parameter()]
        [Alias("SshDir")]
        [string]$Path =
            $(Join-Path $([Environment]::GetFolderPath("ProgramFiles")) `
              "Git\usr\bin"),
        [Parameter()]
        [Switch]$AddAliases = $false
    )

    if (-not $(Test-Path $Path)) {
        $ex = New-Object "IO.DirectoryNotFoundException" `
                         "The directory '$Path' does not exist."

        $er = New-Object System.Management.Automation.ErrorRecord `
                         $ex,"MissingGitDir","ObjectNotFound",$Path

        $PSCmdlet.ThrowTerminatingError($er)
    }

    $commands = @{
        "scp" = "Invoke-SshCopy"
        "sftp" = "Invoke-SshFtp"
        "ssh" = "Invoke-Ssh"
        "ssh-add" = "Invoke-SshAdd"
        "ssh-agent" = "Invoke-SshAgent"
        "ssh-keygen" = "Invoke-SshKeygen"
        "ssh-keyscan" = "Invoke-SshKeyscan"
    }

    # For some reason, using call (&) returns a ton faster than using
    # Start-Process -Wait. We also skip declaring [CmdletBinding()] to get
    # the ease-of-use $Args variable and because we're using the call
    # operator, we don't have to worry about argument count.
    # $program is set inside the loop and is captured using GetNewClosure().
    $command = { & $program $Args }

    foreach ($alias in $commands.Keys) {
        $function = $commands[$alias]
        $program = Join-Path $Path $($alias + ".exe")
        if (Test-Path $program) {
            # Using "function:global" allows us to force the function outside
            # of the module.
            Set-Item -Path $("function:global:" + $function) `
                     -Value $command.GetNewClosure()

            if ($AddAliases) {
                Set-Alias -Name $alias -Value $function `
                          -Option "AllScope" -Scope "Global" -Force
            }
        }
    }
}

<#
.SYNOPSIS
Starts a new SSH Agent process if one is not already running in the current
shell session.

.DESCRIPTION
While the ssh-agent can be started with the 'Invoke-SshAgent' cmdlet, some
environmental variables need to be set for the other SSH processes to pick up
on them and use them. This cmdlet wraps the agent and sets the appropriate
environment variables.

.INPUTS
None

.OUTPUTS
[string[]] Outputs any echoed or unprocessed text from the ssh-agent process.

.NOTES
This will set the SSH_AGENT_PID and SSH_AUTH_SOCK environment variables.
#>
function Start-GitSshAgent {
    [CmdletBinding()]
    param()

    if (-not $(Test-Path "function:global:Invoke-SshAgent")) {
        Write-Error "Run 'Enable-GitSshCommands' before running this cmdlet."
        return
    }

    $pid = $env:SSH_AGENT_PID
    if ($pid -ne $null) {
        $process = Get-Process -Id $pid -ErrorAction SilentlyContinue `
                               -ErrorVariable err
        if ($process -ne $null) {
            "Agent is already running with PID $pid"
            return
        }

        Remove-Item -Path "env:\SSH_AGENT_PID" `
                    -ErrorAction SilentlyContinue `
                    -ErrorVariable err
        Remove-Item -Path "env:\SSH_AUTH_SOCK" `
                    -ErrorAction SilentlyContinue `
                    -ErrorVariable err
    }

    $agentOutput = Invoke-SshAgent -s
    $output = @()
    foreach ($line in $($agentOutput -split '`n')) {
        if ($line -match "(?<name>[^=]+)=(?<val>[^;]+); export \k<name>;") {
            Set-Item -Path "env:\$($matches.name)" -Value $matches.val
            continue
        }

        if ($line -match "^echo (?<output>[^;]+);$") {
            $output += $matches.output
            continue
        }

        $output += $line
    }

    $output
}

<#
.SYNOPSIS
Stops the SSH Agent process if one is running in the current shell session.

.DESCRIPTION
While the ssh-agent can be stopped with the 'Invoke-SshAgent -k' cmdlet, the
environment variables SSH relies on will still be set in the current shell.
This cmdlet wraps the process and cleans up after the agent if the agent was
running.

.INPUTS
None

.OUTPUTS
[string[]] Outputs any echoed or unprocessed text from the ssh-agent process.

.NOTES
This will unset the SSH_AGENT_PID and SSH_AUTH_SOCK environment variables.
#>
function Stop-GitSshAgent {
    [CmdletBinding()]
    param()

    if (-not $(Test-Path "function:global:Invoke-SshAgent")) {
        Write-Error "Run 'Enable-GitSshCommands' before running this cmdlet."
        return
    }

    $pid = $env:SSH_AGENT_PID
    if ($pid -eq $null) {
        Write-Error "ssh-agent is not running in this shell."
        return
    } else {
        $process = Get-Process -Id $pid -ErrorAction SilentlyContinue `
                   -ErrorVariable err
        if ($process -eq $null) {
            Remove-Item -Path "env:\SSH_AGENT_PID" `
                        -ErrorAction SilentlyContinue `
                        -ErrorVariable err
            Remove-Item -Path "env:\SSH_AUTH_SOCK" `
                        -ErrorAction SilentlyContinue `
                        -ErrorVariable err

            Write-Error "ssh-agent is not running in this shell."
            return
        }
    }

    $agentOutput = Invoke-SshAgent -k
    $output = @()
    foreach ($line in $($agentOutput -split '`n')) {
        if ($line -match "^unset (?<name>[^;]+);$") {
            Remove-Item -Path "env:$($matches.name)"
            continue
        }

        if ($line -match "^echo (?<output>[^;]+);$") {
            $output += $matches.output
            continue
        }

        $output += $line
    }

    $output
}

Set-Alias -Name egsc -Value Enable-GitSshCommands
Set-Alias -Name sagsa -Value Start-GitSshAgent
Set-Alias -Name spgsa -Value Stop-GitSshAgent
Export-ModuleMember `
  -Function Enable-GitSshCommands,Start-GitSshAgent,Stop-GitSshAgent `
  -Alias egsc,sagsa,spgsa
